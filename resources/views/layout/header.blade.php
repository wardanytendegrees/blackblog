
        <!--========== HEADER ==========-->
        <header class="header navbar-fixed-top">
            <!-- Navbar -->
            <nav class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>

                        <!-- Logo -->
                        <div class="logo">
                            <a class="logo-wrap" href="index.html">
                                <img class="logo-img logo-img-main" src="img/logo.png" alt="Asentus Logo">
                                <img class="logo-img logo-img-active" src="img/logo-dark.png" alt="Asentus Logo">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container">
                            <ul class="navbar-nav navbar-nav-right">
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == '/' ||Request::path() == 'home' )
                                        active
                                    @endif" href="/home">Home</a></li>
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == 'pricing')
                                        active
                                    @endif " href="/pricing">Pricing</a></li>
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == 'about')
                                        active
                                    @endif
                                 " href="/about">About</a></li>
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == 'product')
                                        active
                                    @endif" href="/product">Products</a></li>
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == 'faq')
                                        active
                                    @endif" href="/faq">FAQ</a></li>
                                <li class="nav-item"><a class="nav-item-child nav-item-hover
                                    @if (Request::path() == 'contact')
                                        active
                                    @endif" href="/contact">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
                </div>
            </nav>
            <!-- Navbar -->
        </header>
