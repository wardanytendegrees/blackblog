<!DOCTYPE html>

<html lang="en" class="no-js">
    <!-- BEGIN HEAD -->
    <html lang="en" class="no-js">
        <!-- BEGIN HEAD -->
@component('layout.head')

@endcomponent
    <!-- END HEAD -->

    <!-- BODY -->
    <body>

@component('layout.header')

@endcomponent
        <!--========== END HEADER ==========-->
@yield('top_content')
@yield('content')
        <!-- End Work -->
        <!--========== END PAGE LAYOUT ==========-->

        <!--========== FOOTER ==========-->
        @component('layout.footer')

        @endcomponent
    </body>
    <!-- END BODY -->
    </html>

